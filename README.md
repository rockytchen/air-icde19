# README #

This is the README file for the Tensorflow implementation of AIR.

### HOWTO ###

(1) The model is implemented and tested with Tensorflow-GPU 1.12.0 and Numpy 1.14.0. We also have NVIDIA-SMI v430.26 and CUDA v10.0 installed.

(2) To test the model, simply run "000_rec_eval_UserAvg.py". We provide pre-trained parameters in the "saved_params" folder.

(3) To re-run the training process with your own data/parameter setting/tasks, modify the model details in both "7_AIR_ML1M_NonLinearAttn.py" and "000_rec_eval_UserAvg.py", and then run "7_AIR_ML1M_NonLinearAttn.py". 
	New parameters will be saved in the "saved_params" folder.
	To test for your own data/parameter/tasks, simply repeat step (2).
	
Note: pay attention to common errors like data format, file path etc. during your modification.

### BIBTEX ###

If you find our work helpful, please cite our paper:

@inproceedings{chen2019air,
  title={AIR: Attentional intention-aware recommender systems},
  author={Chen, Tong and Yin, Hongzhi and Chen, Hongxu and Yan, Rui and Nguyen, Quoc Viet Hung and Li, Xue},
  booktitle={2019 IEEE 35th International Conference on Data Engineering (ICDE)},
  pages={304--315},
  year={2019},
  organization={IEEE}
}