'''
A Dynamic Recurrent Neural Network (LSTM) implementation example using TensorFlow library. 
'''

from __future__ import print_function

import tensorflow as tf
import random
import numpy
import glob
import sklearn
import math
from numpy import genfromtxt
from numpy import array
from sklearn import metrics

from sklearn.preprocessing import label_binarize
from scipy import interp
from itertools import cycle
import matplotlib.pyplot as plt

import matplotlib

print(matplotlib.matplotlib_fname())

import os
#os.environ['CUDA_VISIBLE_DEVICES'] = ''

##############
#data handler#
##############
    
def numpy_softmax(x):
    """Compute softmax values for vector x"""
    return numpy.exp(x) / numpy.sum(numpy.exp(x), axis=0)

'''
    user id ranges from 1 to 943
    item id ranges from 1 to 1682
    rating ranges from 1 to 5
    train_test_flag: str, train or test mode
    file_path: a str/list of file path(s)
    n_action: int, size of action set 
    n_category: int, size of category set
'''
class UserTransReader(object):
    def __init__(self, train_test_flag, file_path, n_action, n_category):
        self.file_path = file_path
        self.data_array = []
        self.batch_id = 0
        self.id_pair = []
        self.maxlen = 0
        self.batch_pi = 0
        self.batch_i = 0
        self.batch_j = 0
        self.batch_n = 0
        self.batch_jt = 0
        self.batch_nt = 0
        self.batch_st = 0
        self.batch_y_AUIA = 0
        self.batch_y_ITFM = 0
        
        data_dic = {} #a dictionary of user transactions
        jt_dic= {}
        nt_dic= {}
        st_dic= {}
        at_dic= {}
        counter = 0
        padder = [0. for x in range(n_action * n_category)]
        if train_test_flag == "train":
            with open(file_path) as in_file:
                for line in in_file: #user_id, movie_cat, movie_id, rating
                    line = line.rstrip().split(",")
                    user_id = int(line[0])
                    if not user_id in data_dic:
                        data_dic[user_id] = []
                    if not user_id in jt_dic:
                        jt_dic[user_id] = []
                    if not user_id in nt_dic:
                        nt_dic[user_id] = []
                    if not user_id in st_dic:
                        st_dic[user_id] = []
                    if not user_id in at_dic:
                        at_dic[user_id] = []
                    movie_cat = int(line[1])
                    nt_dic[user_id].append(movie_cat)
                    movie_id = int(line[2])
                    jt_dic[user_id].append(movie_id)
                    rating = int(line[3])
                    at_dic[user_id].append(rating)
                    intention_onehot = numpy.zeros((n_action, n_category))
                    intention_onehot[rating - 1, movie_cat - 1] = 1
                    intention_onehot = intention_onehot.flatten() #flat the 2d array into a vector
                    st_dic[user_id].append(intention_onehot.tolist().index(1) + 1)
                    #print(intention_onehot.tolist().index(1) + 1)
                    data_dic[user_id].append(intention_onehot.tolist()) #in data dic: key is user id, value is onehot sequence of intention
            max_input_len = int(math.floor(max([len(data_dic[x]) for x in data_dic]) * 0.75))
            for user_id in data_dic:
                pi_seq = data_dic[user_id]
                jt_seq = jt_dic[user_id]
                nt_seq = nt_dic[user_id]
                st_seq = st_dic[user_id]
                at_seq = at_dic[user_id]
                
                input_len = int(math.floor(len(pi_seq) * 0.75))
                input_pi_seq = pi_seq[: input_len]
                input_jt_seq = jt_seq[: input_len]
                input_nt_seq = nt_seq[: input_len]
                input_st_seq = st_seq[: input_len]
                
                label_len = len(pi_seq) - input_len
                label_seq = pi_seq[input_len :] #this is the label for intention prediction
                label = (numpy.sum(numpy.array(label_seq), axis = 0)/float(label_len)).tolist()
                output_pi_seq = pi_seq[input_len :]
                output_jt_seq = jt_seq[input_len :]
                output_nt_seq = nt_seq[input_len :]
                output_st_seq = st_seq[input_len :]
                rating_seq = at_seq[input_len :] #the real rating label
                                
                if input_len < max_input_len:
                    for _ in range(max_input_len - input_len):
                        input_pi_seq.insert(0, padder)
                        input_jt_seq.insert(0, 0)
                        input_nt_seq.insert(0, 0)
                        input_st_seq.insert(0, 0)
                
                '''
                structure of input_seq: [0 input_pi, 1 input_jt, 2 input_nt, 3 input_st, 4 output_jt, 5 output_nt, 6 label_seq, 7 rating_seq, 8 user id]
                '''
                input_seq = [input_pi_seq, input_jt_seq, input_nt_seq, input_st_seq, output_jt_seq, output_nt_seq, label, rating_seq, user_id]
                self.data_array.append(input_seq)
                '''
                release some memory
                '''
                data_dic[user_id] = [] 
                jt_dic[user_id] = []
                nt_dic[user_id] = []
                st_dic[user_id] = []
                
                for x in range(len(output_jt_seq)):
                    self.id_pair.append((counter, x)) # list of tuples    
                counter += 1
                print("users processed: " + str(counter))
            self.maxlen = max_input_len
            random.shuffle(self.id_pair)
        else:
            print("Wrong data flag used.")
        
    def next(self, batch_size):
        batch_id_pair = self.id_pair[self.batch_id:min(self.batch_id + batch_size, len(self.id_pair))]
        current_len = len(batch_id_pair)
        if current_len < batch_size: #leftovers
            batch_id_pair += self.id_pair[: batch_size - current_len]
            print("~ AIR training status>> one epoch for " + str(len(self.id_pair)) + " samples finished.")
            self.batch_id = (batch_size - current_len)
        else:
            self.batch_id = min(self.batch_id + batch_size, len(self.id_pair))
        self.batch_pi = [self.data_array[x[0]][0] for x in batch_id_pair]
        self.batch_y_AUIA = [self.data_array[x[0]][6] for x in batch_id_pair]   
        self.batch_i = [self.data_array[x[0]][8] for x in batch_id_pair]
        self.batch_j = [self.data_array[x[0]][4][x[1]] for x in batch_id_pair]
        self.batch_n = [self.data_array[x[0]][5][x[1]] for x in batch_id_pair]
        self.batch_jt = [self.data_array[x[0]][1] for x in batch_id_pair]
        self.batch_nt = [self.data_array[x[0]][2] for x in batch_id_pair]
        self.batch_st = [self.data_array[x[0]][3] for x in batch_id_pair]
        self.batch_y_ITFM = [self.data_array[x[0]][7][x[1]] for x in batch_id_pair]
        
# ==========
#   MODEL
# ==========

'''
total sample : more than 140,000
- UserIDs range between 1 and 6040 
- MovieIDs range between 1 and 3952
- Ratings are made on a 5-star scale (whole-star ratings only)
- Timestamp is represented in seconds since the epoch as returned by time(2)
- Each user has at least 20 ratings
'''

'''
Parameters
'''
lr = 0.001
batch_size = 200
display_step = 10
max_step = 8000

n_action = 5
n_category = 18
n_item = 3952
n_user = 6040
dim_class = n_action * n_category
dim_hidden = 128
dim_embedding = 128

train_data = UserTransReader("train", "200len_user_transaction_train.csv", n_action, n_category)
maxlen = train_data.maxlen
'''
construct a index padding scheme
'''
pad_1_to_2_index = []
for a in range(maxlen - 1):
    b = a + 1
    while b <= (maxlen - 1):
        pad_1_to_2_index.append([a, b])
        b += 1
pad_batch_with_index = []
for a in range(batch_size):
    for b in range(maxlen):
        pad_batch_with_index.append([a, b])

'''
tf graph input
'''
pi = tf.placeholder("float", [None, None, dim_class]) #(batch, seqlen, dim_class)
i = tf.placeholder(tf.int32, [None]) #(batch), a 1d vector representing the index of each user (starting from 1), u_i
j = tf.placeholder(tf.int32, [None]) #(batch), a 1d vector representing the index of each targeted item (starting from 1), v_j
n = tf.placeholder(tf.int32, [None]) #(batch), a 1d vector representing the index of each targeted category (starting from 1), c_n
jt = tf.placeholder(tf.int32, [None, None]) #(batch, seqlen), a 2d vector representing the index sequence of interacted item for each user, v_jt 
nt = tf.placeholder(tf.int32, [None, None]) #(batch, seqlen), a 2d vector representing the index sequence of interacted category for each user, c_nt
st = tf.placeholder(tf.int32, [None, None]) #(batch, batch), a 2d vector representing the index sequence of interacted category for each user, pi_st
y_intention = tf.placeholder("float", [None, dim_class]) #(batch, dim_class), label for AUIA probability distribution
y_rating = tf.placeholder("float", [None])

'''
dictionaries for trainable weights and biases
'''
weights = {
    'intention_embedding': tf.Variable(tf.random_normal([dim_class, dim_embedding]), name = "intention_embedding", trainable = True),
    'matrix_theta': tf.Variable(tf.constant(round(1.0/dim_class, 2), shape = [dim_class, dim_class]), name = "matrix_theta", trainable = True),
    'w_e': tf.Variable(tf.random_normal([dim_hidden, 1]), name = "w_e", trainable = True),
    'w_item': tf.Variable(tf.random_normal([n_item, 1]), name = "w_item", trainable = True),
    'w_user': tf.Variable(tf.random_normal([n_user, 1]), name = "w_user", trainable = True),
    'item_lookup': tf.Variable(tf.constant(0., shape = [n_item, dim_hidden]), name = "item_lookup", trainable = True),
    'user_lookup': tf.Variable(tf.constant(0., shape = [n_user, dim_hidden]), name = "user_lookup", trainable = True)
}
biases = {
    'b_e': tf.Variable(tf.random_normal([dim_class, 1]), name = "b_e", trainable = True),
    'b_u': tf.Variable(tf.random_normal([n_user, 1]), name = "b_u", trainable = True),
    'b_0': tf.Variable(tf.random_normal([1, 1]), name = "b_0", trainable = True)
}

'''
AIR main structure
'''
def model_AIR(pi, i, j, n, jt, nt, st, weights, biases):
    pi_flatten = tf.reshape(pi, [-1, dim_class]) #(batch*seqlen, dim_class)
    x = tf.reshape(tf.matmul(pi_flatten, weights['intention_embedding']), [-1, maxlen, dim_embedding]) #(batch, seqlen, dim_embedding)
    '''
    here starts AUIA
    3 layer structure
    '''
    lstm_cell_layer1 = tf.contrib.rnn.BasicLSTMCell(dim_hidden, forget_bias = 1, activation = tf.sigmoid, state_is_tuple = True, reuse = None)
    lstm_cell_layer2 = tf.contrib.rnn.BasicLSTMCell(dim_hidden, forget_bias = 1, activation = tf.sigmoid, state_is_tuple = True, reuse = None)
    lstm_cell_layer3 = tf.contrib.rnn.BasicLSTMCell(dim_hidden, forget_bias = 1, activation = tf.sigmoid, state_is_tuple = True, reuse = None)

    lstm_signal_cell = tf.contrib.rnn.MultiRNNCell([lstm_cell_layer1, lstm_cell_layer2, lstm_cell_layer3], state_is_tuple = True)

    with tf.variable_scope('lstm_signal_cell'):
        h, final_state = tf.nn.dynamic_rnn(lstm_signal_cell, x, dtype = tf.float32) #h:(batch, seqlen, dim_hidden)

    this_theta = tf.nn.relu(weights['matrix_theta'])
    theta = tf.reshape(tf.matmul(pi_flatten, this_theta), [-1, dim_class, 1]) #(batch*seqlen, dim_class, 1)
    h = tf.reshape(h, [-1, 1, dim_hidden]) #(batch*seqlen, 1, dim_hidden)
    H = tf.reduce_sum(tf.reshape(tf.matmul(theta, h), [-1, maxlen, dim_class, dim_hidden]), axis = 1) #(batch, dim_class, dim_hidden)
    e = tf.matmul(tf.reshape(H, [-1, dim_hidden]), weights['w_e']) + tf.tile(biases['b_e'], [batch_size, 1]) #(batch*dim_class, 1)
    e = tf.reshape(e, [-1, dim_class]) #(batch, dim_class)
    gamma = tf.nn.softmax(e, dim = -1) #(batch, dim_class)
    
    '''
    algorithm 1 for categorical intention aggregation
    '''
    pi_aggregated = tf.reshape(tf.reduce_sum(pi, axis = 1), [-1, n_action, n_category]) #(batch, n_action, n_category)
    pi_weighted = pi_aggregated / tf.tile((tf.reduce_sum(pi_aggregated, axis = 1, keep_dims = True) + 0.001), [1, n_action, 1]) #(batch, n_action, n_category)
    gamma_aggregated = tf.reduce_sum(tf.multiply(tf.reshape(gamma, [-1, n_action, n_category]), pi_weighted), axis = 1) #(batch, n_category)
    #gamma_aggregated = tf.nn.softmax(gamma_aggregated, dim = -1)
    '''
    here starts ITFM
    padding the weight vector and matrices for index 0
    '''
    w_item = tf.concat([tf.constant([[0.]]), weights['w_item']], axis = 0) #(n_item+1, 1)
    theta_expanded = tf.concat([tf.constant(0., shape = [dim_class + 1, 1]), tf.concat([tf.constant(0., shape = [1, dim_class]), this_theta], axis = 0)], axis = 1) #(dim_class+1, dim_class+1)
    item_lookup = tf.concat([tf.constant(0., shape = [1, dim_hidden]), weights['item_lookup']], axis = 0) #(n_item+1, dim_hidden)
    user_lookup = tf.concat([tf.constant(0., shape = [1, dim_hidden]), weights['item_lookup']], axis = 0) #(n_user+1, dim_hidden)
    gamma_aggregated = tf.concat([tf.constant(0., shape = [batch_size, 1]), gamma_aggregated], axis = 1) #(batch, n_category + 1)
    len_each_seq = tf.cast(tf.count_nonzero(jt, 1, keep_dims=True), dtype=tf.float32) #(batch, 1), the real T of each sequence
    '''`
    first term of ITFM
    '''
    y_1 = tf.tile(biases['b_0'], [batch_size, 1]) #(batch, 1), the global bias
    '''
    second term of ITFM
    '''
    y_2 = tf.reshape(tf.gather(biases['b_u'], i, axis = 0), [-1, 1]) #(batch, 1) the user bias
    '''
    third term of ITFM
    '''
    w_all_jt = tf.reshape(tf.gather(w_item, tf.reshape(jt, [-1]), axis = 0), [-1, 1]) #(batch*seqlen, 1)
    batch_id = tf.tile(tf.reshape(tf.constant([x for x in range(batch_size)], dtype = tf.int32), [-1, 1, 1]), [1, maxlen, 1]) #(batch, seqlen, 1)
    nt_res = tf.reshape(nt, [-1, maxlen, 1]) #(batch, seqlen, 1)
    nt_pair = tf.concat([batch_id, nt_res], axis = 2) #(batch, seqlen, 2)
    gamma_all_nt = tf.reshape(tf.gather_nd(gamma_aggregated, tf.reshape(nt_pair, [-1, 2])), [-1, 1]) #(batch*seqlen, 1)
    y_3 = tf.reduce_sum(tf.reshape(tf.multiply(w_all_jt, gamma_all_nt), [-1, maxlen]), axis = 1, keep_dims = True) #(batch, 1)
    y_3 = tf.div(y_3, len_each_seq)
    '''
    forth term of ITFM
    '''
    n_target = tf.concat([tf.constant([[x] for x in range(batch_size)]), tf.reshape(n, [-1, 1])], axis = 1) #(batch, 2)
    y_4 = tf.reshape(tf.multiply(tf.gather(w_item, j, axis = 0), tf.reshape(tf.gather_nd(gamma_aggregated, tf.reshape(n_target, [-1, 2])), [-1, 1])), [-1, 1]) #(batch, 1)
    '''
    fifth term of ITFM
    '''
    v_j_target = tf.reshape(tf.gather(item_lookup, j, axis = 0), [-1, dim_hidden, 1]) #(batch, dim_hidden, 1)
    u_i_target = tf.reshape(tf.gather(user_lookup, i, axis = 0), [-1, 1, dim_hidden]) #(batch, 1, dim_hidden)
    y_5 = tf.multiply(tf.reshape(tf.matmul(u_i_target, v_j_target), [-1, 1]), tf.reshape(tf.gather_nd(gamma_aggregated, tf.reshape(n_target, [-1, 2])), [-1, 1])) #(batch, 1)    
    '''
    sixth term of ITFM
    '''
    v_all_jt = tf.reshape(tf.gather(item_lookup, tf.reshape(jt, [-1]), axis = 0), [-1, 1, dim_hidden]) #(batch*seqlen, 1, dim_hidden)
    u_all_target = tf.reshape(tf.tile(u_i_target, [1, maxlen, 1]), [-1, dim_hidden, 1]) #(batch*seqlen, dim_hidden, 1)
    y_6 = tf.reduce_sum(tf.reshape(tf.multiply(tf.reshape(tf.matmul(v_all_jt, u_all_target), [-1, 1]), gamma_all_nt), [-1, maxlen]), axis = 1, keep_dims = True) #(batch, 1)
    y_6 = tf.div(y_6, len_each_seq) #(batch, 1)
    '''
    seventh term of ITFM
    ''' 
    j_first_pair = tf.constant(pad_batch_with_index, dtype = tf.int32) #(batch*seqlen, 2)
    j_first_index = tf.reshape(tf.gather_nd(jt, j_first_pair), [-1]) #(batch*seqlen)
    v_all_first_jt = tf.reshape(tf.gather(item_lookup, j_first_index, axis = 0), [-1, 1, dim_hidden]) #(batch*seqlen, 1, dim_hidden)
    #print(v_all_first_jt.shape)
    v_all_target_j = tf.reshape(tf.gather(item_lookup, tf.tile(j, [maxlen]), axis = 0), [-1, dim_hidden, 1]) #(batch*seqlen, dim_hidden, 1)
    #print(v_all_target_j.shape)
    theta_sum = tf.constant(0., shape = [batch_size, maxlen]) #(batch,seqlen)
    for each_action in range(n_action):
        s_target = tf.multiply(tf.constant([n_action for x in range(batch_size)], dtype = tf.int32), n_category) + 1 #(batch)
        s_index_pair = tf.concat([tf.reshape(st, [-1, maxlen, 1]), tf.tile(tf.reshape(s_target, [-1, 1, 1]), [1, maxlen, 1])], axis = 2) #(batch, seqlen, 2)
        theta_sum = theta_sum + tf.reshape(tf.gather_nd(theta_expanded, tf.reshape(s_index_pair, [-1, 2])), [-1, maxlen]) #(batch, seqlen)
    y_7 = tf.reduce_sum(tf.multiply(tf.reshape(tf.matmul(v_all_first_jt, v_all_target_j), [-1, maxlen]), theta_sum), axis = 1, keep_dims = True) #(batch, 1)
    y_7 = tf.div(y_7, (len_each_seq * n_action)) #(batch, 1)
    #print(y_7.shape)
    '''
    eigth term of ITFM
    '''   
    jt_pair_in_seq = tf.reshape(tf.constant(pad_1_to_2_index, dtype = tf.int32), [1, -1, 2]) #(1, sum[maxlen - 1], 2)
    jt_first_in_seq, jt_second_in_seq = tf.split(tf.tile(jt_pair_in_seq, [batch_size, 1, 1]), [1, 1], axis = 2) #(batch, sum[maxlen - 1], 1)
    batch_id = tf.tile(tf.constant([[[x]] for x in range(batch_size)], dtype = tf.int32), [1, tf.shape(jt_pair_in_seq)[1], 1]) #(batch, sum[maxlen - 1], 1)
    jt_first_in_seq = tf.concat([batch_id, jt_first_in_seq], axis = 2) #(batch, sum[maxlen - 1], 2)
    jt_second_in_seq = tf.concat([batch_id, jt_second_in_seq], axis = 2) #(batch, sum[maxlen - 1], 2)
    jt_first_index = tf.reshape(tf.gather_nd(jt, tf.reshape(jt_first_in_seq, [-1, 2])), [-1, 1]) #(batch*sum[maxlen - 1], 1)
    jt_second_index = tf.reshape(tf.gather_nd(jt, tf.reshape(jt_second_in_seq, [-1, 2])), [-1, 1]) #(batch*sum[maxlen - 1], 1)
    v_all_first_jt = tf.reshape(tf.gather(item_lookup, tf.reshape(jt_first_index, [-1]), axis = 0), [-1, 1, dim_hidden]) #((batch*sum[maxlen - 1], 1, dim_hidden)
    v_all_second_jt = tf.reshape(tf.gather(item_lookup, tf.reshape(jt_second_index, [-1]), axis = 0), [-1, dim_hidden, 1]) #((batch*sum[maxlen - 1], dim_hidden, 1)
        
    s_first_index = tf.reshape(tf.gather_nd(st, tf.reshape(jt_first_in_seq, [-1, 2])), [-1, 1]) #(batch*sum[maxlen - 1], 1)
    s_second_index = tf.reshape(tf.gather_nd(st, tf.reshape(jt_second_in_seq, [-1, 2])), [-1, 1]) #(batch*sum[maxlen - 1], 1)
    st_pair_index = tf.concat([s_first_index, s_second_index], axis = 1) #(batch*sum[maxlen - 1], 2)
    theta_all_jt = tf.reshape(tf.gather_nd(theta_expanded, st_pair_index), [-1, 1]) #(batch*sum[maxlen - 1], 1)
    y_8 = tf.reduce_sum(tf.reshape(tf.multiply(tf.reshape(tf.matmul(v_all_first_jt, v_all_second_jt), [-1, 1]), theta_all_jt), [batch_size, -1]), axis = 1, keep_dims = True) #(batch, 1)
    super_len_each_seq = tf.multiply(len_each_seq, (len_each_seq - 1)) / 2 #(batch, 1)
    y_8 = tf.div(y_8, super_len_each_seq) #(batch, 1) 
    '''
    combine all inputs and form the result of ITFM
    '''
    rating_p = y_1 + y_2 + y_3 + y_4 + y_5 + y_6 + y_7 + y_8#(batch, 1) #
    
    return e, this_theta, rating_p

e, matrix_theta, rating_p = model_AIR(pi, i, j, n, jt, nt, st, weights, biases)

# Define loss and optimizer
lamda_1 = 5.0
lamda_2 = 0.01 # a big lamda prevents model from overfitting training data; a small one prevents underfitting
lamda_3 = 0.001
l2 = sum(tf.nn.l2_loss(tf_var) for tf_var in tf.trainable_variables()) # L2 regularization to prevent overfitting

cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels = y_intention, logits = e, dim = -1))
attn_constraint = tf.reduce_sum(tf.square(tf.constant([1. for x in range(dim_class)]) - tf.reduce_sum(matrix_theta, axis = 1)))

loss_function_AUIA = lamda_1 * cross_entropy + lamda_2 * l2
loss_function_ITFM = tf.reduce_mean(tf.square(tf.subtract(rating_p, y_rating))) #squared error
loss_function = loss_function_AUIA + loss_function_ITFM + lamda_3 * attn_constraint

optimizer_AIR = tf.train.AdamOptimizer(learning_rate = lr).minimize(loss_function)
#optimizer_AIR = tf.train.GradientDescentOptimizer(learning_rate = lr).minimize(loss_function_ITFM)

print("TF graph loaded. Start training now.")
# Launch the traning session

intention_loss_file = open("intention_loss_file.csv", "w")
recommendation_loss_file = open("recommendation_loss_file.csv", "w")

saver = tf.train.Saver()
with tf.Session() as sess:
    '''
    initialize all parameters before the training loop starts
    '''
    sess.run(tf.global_variables_initializer())
    sess.run(tf.local_variables_initializer())
    step = 1
    
    '''
    keep training until max step is reached
    ''' 
    
    while step <= max_step:
        
        train_data.next(batch_size)
        sess.run(optimizer_AIR, feed_dict={pi: train_data.batch_pi,
                                             i: train_data.batch_i,
                                             j: train_data.batch_j,
                                             n: train_data.batch_n,
                                            jt: train_data.batch_jt,
                                            nt: train_data.batch_nt,
                                            st: train_data.batch_st,
                                   y_intention: train_data.batch_y_AUIA,      
                                      y_rating: train_data.batch_y_ITFM})
    
        if step % display_step == 0:
            rating, loss_combine, loss_intention, loss_rating, tht = sess.run([rating_p, loss_function, loss_function_AUIA, loss_function_ITFM, matrix_theta], 
                                                                    feed_dict = {pi: train_data.batch_pi,
                                                                                  i: train_data.batch_i,
                                                                                  j: train_data.batch_j,
                                                                                  n: train_data.batch_n,
                                                                                 jt: train_data.batch_jt,
                                                                                 nt: train_data.batch_nt,
                                                                                 st: train_data.batch_st,
                                                                        y_intention: train_data.batch_y_AUIA,
                                                                           y_rating: train_data.batch_y_ITFM})
            
            intention_loss_file.write(str(loss_intention) + "\n")
            recommendation_loss_file.write(str(loss_rating) + "\n")
            #print(gga)
            #print(rating.tolist())
            #print(train_data.batch_y_ITFM)
            print("AIR training status>> " +
                  "step: " + str(step) + 
                  #";  te.sub.t acc, prec, rec: " + "{:.3f}".format(test_sub_accuracy) + 
                  #", " + "{:.3f}".format(test_sub_precision) + ", " + "{:.3f}".format(test_sub_recall) + 
                  "; combined loss: " + "{:.3f}".format(loss_combine) +
                  "; intention prediction loss: " + "{:.3f}".format(loss_intention) +
                  "; recommendation loss: " + "{:.3f}".format(loss_rating)
                  )
            print("Intention Matrix Theta:")
            print(tht)
                  
        if step % 1000 == 0:
            save_path = saver.save(sess, "saved_params/version7/model_" + str(step) + ".ckpt")
            print("~ AIR training status>> model saved in saved_params floder.")          
        step += 1
    
    print("Status>> trainig finished.")

intention_loss_file.close()
recommendation_loss_file.close()