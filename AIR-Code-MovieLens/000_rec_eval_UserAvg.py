'''
A Dynamic Recurrent Neural Network (LSTM) implementation example using TensorFlow library. 
'''

from __future__ import print_function

import tensorflow as tf
import random
import numpy
import glob
import sklearn
import math
from numpy import genfromtxt
from numpy import array
from sklearn import metrics

from sklearn.preprocessing import label_binarize
from scipy import interp
from itertools import cycle
import matplotlib.pyplot as plt

import matplotlib

print(matplotlib.matplotlib_fname())

import os
os.environ['CUDA_VISIBLE_DEVICES'] = '0'

##############
#data handler#
##############
    
def numpy_softmax(x):
    """Compute softmax values for vector x"""
    return numpy.exp(x) / numpy.sum(numpy.exp(x), axis=0)

'''
    user id ranges from 1 to 943
    item id ranges from 1 to 1682
    rating ranges from 1 to 5
    train_test_flag: str, train or test mode
    file_path: a str/list of file path(s)
    n_action: int, size of action set 
    n_category: int, size of category set
'''
class UserTransReader(object):
    def __init__(self, train_test_flag, file_path, cat_file_path, n_action, n_category):
        self.file_path = file_path
        self.data_array = []
        self.batch_id = 0
        self.id_pair = []
        self.maxlen = 0
        self.batch_pi = 0
        self.batch_i = 0
        self.batch_j = 0
        self.batch_n = 0
        self.batch_jt = 0
        self.batch_nt = 0
        self.batch_st = 0
        self.batch_y_AUIA = 0
        self.batch_y_ITFM = 0
        self.output_len_dic = {}
        self.negative_sample = {}
        
        data_dic = {} #a dictionary of user transactions
        jt_dic= {}
        nt_dic= {}
        st_dic= {}
        at_dic= {}
        input_len_dic = {}
        counter = 0
        padder = [0. for x in range(n_action * n_category)]
        if train_test_flag == "test":
            with open(file_path[0]) as in_file:
                for line in in_file: #user_id, movie_cat, movie_id, rating
                    line = line.rstrip().split(",")
                    user_id = int(line[0])
                    if not user_id in data_dic:
                        data_dic[user_id] = []
                    if not user_id in jt_dic:
                        jt_dic[user_id] = []
                    if not user_id in nt_dic:
                        nt_dic[user_id] = []
                    if not user_id in st_dic:
                        st_dic[user_id] = []
                    if not user_id in at_dic:
                        at_dic[user_id] = []                        
                    movie_cat = int(line[1])
                    nt_dic[user_id].append(movie_cat)
                    movie_id = int(line[2])
                    jt_dic[user_id].append(movie_id)
                    rating = int(line[3])
                    at_dic[user_id].append(rating)
                    intention_onehot = numpy.zeros((n_action, n_category))
                    intention_onehot[rating - 1, movie_cat - 1] = 1
                    intention_onehot = intention_onehot.flatten() #flat the 2d array into a vector
                    st_dic[user_id].append(intention_onehot.tolist().index(1) + 1)
                    #print(intention_onehot.tolist().index(1) + 1)                    
                    data_dic[user_id].append(intention_onehot.tolist()) #in data dic: key is user id, value is onehot sequence of intention
            max_input_len = max([len(data_dic[x]) for x in data_dic])
            max_user_id = max([x for x in data_dic])
            for user_id in data_dic:
                input_len_dic[user_id] = len(data_dic[user_id])
            with open(file_path[1]) as in_file: #user_id, movie_cat, movie_id, rating
                for line in in_file:
                    line = line.rstrip().split(",")
                    user_id = int(line[0])
                    if not user_id in data_dic:
                        data_dic[user_id] = []
                    if not user_id in at_dic:
                        at_dic[user_id] = []
                    movie_cat = int(line[1])
                    nt_dic[user_id].append(movie_cat)
                    movie_id = int(line[2])
                    jt_dic[user_id].append(movie_id)
                    rating = int(line[3])
                    at_dic[user_id].append(rating)
                    intention_onehot = numpy.zeros((n_action, n_category))
                    intention_onehot[rating - 1, movie_cat - 1] = 1
                    intention_onehot = intention_onehot.flatten() #flat the 2d array into a vector
                    st_dic[user_id].append(intention_onehot.tolist().index(1) + 1)
                    #print(intention_onehot.tolist().index(1) + 1)                     
                    data_dic[user_id].append(intention_onehot.tolist()) #in data dic: key is user id, value is onehot sequence of intention
            item_cat = {}
            with open(cat_file_path) as in_file:
                for line in in_file:
                    line = line.rstrip().split(",")
                    item_cat[int(line[0])] = int(line[1])     
            for user_id in range(1, max_user_id + 1):
                u_v_neg = [x for x in item_cat]
                #for key in range(1, 3953):
                #   if x in item_cat:
                #       u_v_neg.append(x)   
                pi_seq = data_dic[user_id]
                jt_seq = jt_dic[user_id]
                nt_seq = nt_dic[user_id]
                st_seq = st_dic[user_id]
                at_seq = at_dic[user_id]
                for each_item in list(set(jt_seq)):
                    idx = u_v_neg.index(each_item)
                    del u_v_neg[idx]
                random.shuffle(u_v_neg)
                neg_item_list = u_v_neg[:100] #noise sample number
                self.negative_sample[user_id] = [[x, item_cat[x]] for x in neg_item_list] #[item, cat] list
                
                input_len = input_len_dic[user_id] #switch between 20% hold-out validation and leave-one-out validation
                #input_len = len(pi_seq) - 1
                input_pi_seq = pi_seq[: input_len]
                input_jt_seq = jt_seq[: input_len]
                input_nt_seq = nt_seq[: input_len]
                input_st_seq = st_seq[: input_len]
                
                label_len = len(pi_seq) - input_len
                self.output_len_dic[user_id] = label_len
                label_seq = pi_seq[input_len :] #this is the label for intention prediction
                label = (numpy.sum(numpy.array(label_seq), axis = 0)/float(label_len)).tolist()
                output_pi_seq = pi_seq[input_len :]
                output_jt_seq = jt_seq[input_len :]
                output_nt_seq = nt_seq[input_len :]
                output_st_seq = st_seq[input_len :]
                rating_seq = at_seq[input_len :] #the real rating label
                           
                if input_len < max_input_len:
                    for _ in range(max_input_len - input_len):
                        input_pi_seq.insert(0, padder)
                        input_jt_seq.insert(0, 0)
                        input_nt_seq.insert(0, 0)
                        input_st_seq.insert(0, 0)
                
                '''
                structure of input_seq: [0 input_pi, 1 input_jt, 2 input_nt, 3 input_st, 4 output_jt, 5 output_nt, 6 label_seq, 7 rating_seq, 8 user id]
                '''
                input_seq = [input_pi_seq, input_jt_seq, input_nt_seq, input_st_seq, output_jt_seq, output_nt_seq, label, rating_seq, user_id]
                self.data_array.append(input_seq)
                '''
                release some memory
                '''
                data_dic[user_id] = [] 
                jt_dic[user_id] = []
                nt_dic[user_id] = []
                st_dic[user_id] = []
                
                for x in range(len(output_jt_seq)):
                    self.id_pair.append((counter, x)) # list of tuples
                counter += 1
                print("users processed: " + str(counter))
            self.maxlen = max_input_len
        else:
            print("Wrong data flag used.")
        
    def next(self):
        #batch_id_pair = self.id_pair[self.batch_id:min(self.batch_id + batch_size, len(self.id_pair))]
        #current_len = len(batch_id_pair)
        #if current_len < batch_size: #leftovers
        #   batch_id_pair += self.id_pair[: batch_size - current_len]
        #   print("~ AIR training status>> one epoch for " + str(len(self.id_pair)) + " samples finished.")
        #   self.batch_id = (batch_size - current_len)
        #else:
        #   self.batch_id = min(self.batch_id + batch_size, len(self.id_pair))
        self.batch_pi = numpy.array([self.data_array[self.batch_id][0]])
        self.batch_i = numpy.array([self.data_array[self.batch_id][8]])
        self.batch_j = numpy.array(self.data_array[self.batch_id][4])
        self.batch_n = numpy.array(self.data_array[self.batch_id][5])
        self.batch_jt = numpy.array([self.data_array[self.batch_id][1]])
        self.batch_nt = numpy.array([self.data_array[self.batch_id][2]])
        self.batch_st = numpy.array([self.data_array[self.batch_id][3]])
        self.batch_id += 1
        
# ==========
#   MODEL
# ==========

# Parameters
lr = 0.001
batch_size = 100
display_step = 10
max_step = 24000

n_action = 5
n_category = 18
n_item = 3952
n_user = 6040
dim_class = n_action * n_category
dim_hidden = 128
dim_embedding = 128

test_data = UserTransReader("test", ["200len_user_transaction_train.csv", "200len_user_transaction_test.csv"], "movie_category.csv", n_action, n_category)
maxlen = test_data.maxlen
'''
construct a index padding scheme
'''
pad_1_to_2_index = []
for a in range(maxlen - 1):
    b = a + 1
    while b <= (maxlen - 1):
        pad_1_to_2_index.append([a, b])
        b += 1
pad_batch_with_index = []
for a in range(batch_size):
    for b in range(maxlen):
        pad_batch_with_index.append([a, b])

'''
tf graph input
'''
pi = tf.placeholder("float", [None, None, dim_class]) #(batch, seqlen, dim_class)
i = tf.placeholder(tf.int32, [None]) #(batch), a 1d vector representing the index of each user (starting from 1), u_i
j = tf.placeholder(tf.int32, [None]) #(batch), a 1d vector representing the index of each targeted item (starting from 1), v_j
n = tf.placeholder(tf.int32, [None]) #(batch), a 1d vector representing the index of each targeted category (starting from 1), c_n
jt = tf.placeholder(tf.int32, [None, None]) #(batch, seqlen), a 2d vector representing the index sequence of interacted item for each user, v_jt 
nt = tf.placeholder(tf.int32, [None, None]) #(batch, seqlen), a 2d vector representing the index sequence of interacted category for each user, c_nt
st = tf.placeholder(tf.int32, [None, None]) #(batch, batch), a 2d vector representing the index sequence of interacted category for each user, pi_st

'''
dictionaries for trainable weights and biases
'''
weights = {
    'intention_embedding': tf.get_variable("intention_embedding", shape = [dim_class, dim_embedding]),
    'matrix_theta': tf.get_variable("matrix_theta", shape = [dim_class, dim_class]),
    'w_e': tf.get_variable("w_e", shape = [dim_hidden, 1]),
    'w_item': tf.get_variable("w_item", shape = [n_item, 1]),
    'w_user': tf.get_variable("w_user", shape = [n_user, 1]),
    'item_lookup': tf.get_variable("item_lookup", shape = [n_item, dim_hidden]),
    'user_lookup': tf.get_variable("user_lookup", shape = [n_user, dim_hidden])
}
biases = {
    'b_e': tf.get_variable("b_e", shape = [dim_class, 1]),
    'b_u': tf.get_variable("b_u", shape = [n_user, 1]),
    'b_0': tf.get_variable("b_0", shape = [1, 1])
}

def model_AIR(pi, i, j, n, jt, nt, st, weights, biases):
    pi_flatten = tf.reshape(pi, [-1, dim_class]) #(batch*seqlen, dim_class)
    x = tf.reshape(tf.matmul(pi_flatten, weights['intention_embedding']), [-1, maxlen, dim_embedding]) #(batch, seqlen, dim_embedding)
    '''
    here starts AUIA
    3 layer structure
    '''
    lstm_cell_layer1 = tf.contrib.rnn.BasicLSTMCell(dim_hidden, forget_bias = 1, activation = tf.sigmoid, state_is_tuple = True, reuse = None)
    lstm_cell_layer2 = tf.contrib.rnn.BasicLSTMCell(dim_hidden, forget_bias = 1, activation = tf.sigmoid, state_is_tuple = True, reuse = None)
    lstm_cell_layer3 = tf.contrib.rnn.BasicLSTMCell(dim_hidden, forget_bias = 1, activation = tf.sigmoid, state_is_tuple = True, reuse = None)

    lstm_signal_cell = tf.contrib.rnn.MultiRNNCell([lstm_cell_layer1, lstm_cell_layer2, lstm_cell_layer3], state_is_tuple = True)

    with tf.variable_scope('lstm_signal_cell'):
        h, final_state = tf.nn.dynamic_rnn(lstm_signal_cell, x, dtype = tf.float32) #h:(batch, seqlen, dim_hidden)

    this_theta = tf.nn.relu(weights['matrix_theta'])
    theta = tf.reshape(tf.matmul(pi_flatten, this_theta), [-1, dim_class, 1]) #(batch*seqlen, dim_class, 1)
    h = tf.reshape(h, [-1, 1, dim_hidden]) #(batch*seqlen, 1, dim_hidden)
    H = tf.reduce_sum(tf.reshape(tf.matmul(theta, h), [-1, maxlen, dim_class, dim_hidden]), axis = 1) #(batch, dim_class, dim_hidden)
    e = tf.matmul(tf.reshape(H, [-1, dim_hidden]), weights['w_e']) + tf.tile(biases['b_e'], [batch_size, 1]) #(batch*dim_class, 1)
    e = tf.reshape(e, [-1, dim_class]) #(batch, dim_class)
    gamma = tf.nn.softmax(e, dim = -1) #(batch, dim_class)
    
    '''
    algorithm 1 for categorical intention aggregation
    '''
    pi_aggregated = tf.reshape(tf.reduce_sum(pi, axis = 1), [-1, n_action, n_category]) #(batch, n_action, n_category)
    pi_weighted = pi_aggregated / tf.tile((tf.reduce_sum(pi_aggregated, axis = 1, keep_dims = True) + 0.001), [1, n_action, 1]) #(batch, n_action, n_category)
    gamma_aggregated = tf.reduce_sum(tf.multiply(tf.reshape(gamma, [-1, n_action, n_category]), pi_weighted), axis = 1) #(batch, n_category)
    
    '''
    here starts ITFM
    padding the weight vector and matrices for index 0
    '''
    w_item = tf.concat([tf.constant([[0.]]), weights['w_item']], axis = 0) #(n_item+1, 1)
    theta_expanded = tf.concat([tf.constant(0., shape = [dim_class + 1, 1]), tf.concat([tf.constant(0., shape = [1, dim_class]), this_theta], axis = 0)], axis = 1) #(dim_class+1, dim_class+1)
    item_lookup = tf.concat([tf.constant(0., shape = [1, dim_hidden]), weights['item_lookup']], axis = 0) #(n_item+1, dim_hidden)
    user_lookup = tf.concat([tf.constant(0., shape = [1, dim_hidden]), weights['item_lookup']], axis = 0) #(n_user+1, dim_hidden)
    gamma_aggregated = tf.concat([tf.constant(0., shape = [batch_size, 1]), gamma_aggregated], axis = 1) #(batch, n_category + 1)
    len_each_seq = tf.cast(tf.count_nonzero(jt, 1, keep_dims=True), dtype=tf.float32) #(batch, 1), the real T of each sequence
    '''`
    first term of ITFM
    '''
    y_1 = tf.tile(biases['b_0'], [batch_size, 1]) #(batch, 1), the global bias
    '''
    second term of ITFM
    '''
    y_2 = tf.reshape(tf.gather(biases['b_u'], i, axis = 0), [-1, 1]) #(batch, 1) the user bias
    '''
    third term of ITFM
    '''
    w_all_jt = tf.reshape(tf.gather(w_item, tf.reshape(jt, [-1]), axis = 0), [-1, 1]) #(batch*seqlen, 1)
    batch_id = tf.tile(tf.reshape(tf.constant([x for x in range(batch_size)], dtype = tf.int32), [-1, 1, 1]), [1, maxlen, 1]) #(batch, seqlen, 1)
    nt_res = tf.reshape(nt, [-1, maxlen, 1]) #(batch, seqlen, 1)
    nt_pair = tf.concat([batch_id, nt_res], axis = 2) #(batch, seqlen, 2)
    gamma_all_nt = tf.reshape(tf.gather_nd(gamma_aggregated, tf.reshape(nt_pair, [-1, 2])), [-1, 1]) #(batch*seqlen, 1)
    y_3 = tf.reduce_sum(tf.reshape(tf.multiply(w_all_jt, gamma_all_nt), [-1, maxlen]), axis = 1, keep_dims = True) #(batch, 1)
    y_3 = tf.div(y_3, len_each_seq)
    '''
    forth term of ITFM
    '''
    #print(n.shape)
    n_target = tf.concat([tf.constant([[x] for x in range(batch_size)]), tf.reshape(n, [-1, 1])], axis = 1) #(batch, 2)
    y_4 = tf.reshape(tf.multiply(tf.gather(w_item, j, axis = 0), tf.reshape(tf.gather_nd(gamma_aggregated, tf.reshape(n_target, [-1, 2])), [-1, 1])), [-1, 1]) #(batch, 1)
    '''
    fifth term of ITFM
    '''
    v_j_target = tf.reshape(tf.gather(item_lookup, j, axis = 0), [-1, dim_hidden, 1]) #(batch, dim_hidden, 1)
    u_i_target = tf.reshape(tf.gather(user_lookup, i, axis = 0), [-1, 1, dim_hidden]) #(batch, 1, dim_hidden)
    y_5 = tf.multiply(tf.reshape(tf.matmul(u_i_target, v_j_target), [-1, 1]), tf.reshape(tf.gather_nd(gamma_aggregated, tf.reshape(n_target, [-1, 2])), [-1, 1])) #(batch, 1)    
    '''
    sixth term of ITFM
    '''
    v_all_jt = tf.reshape(tf.gather(item_lookup, tf.reshape(jt, [-1]), axis = 0), [-1, 1, dim_hidden]) #(batch*seqlen, 1, dim_hidden)
    u_all_target = tf.reshape(tf.tile(u_i_target, [1, maxlen, 1]), [-1, dim_hidden, 1]) #(batch*seqlen, dim_hidden, 1)
    y_6 = tf.reduce_sum(tf.reshape(tf.multiply(tf.reshape(tf.matmul(v_all_jt, u_all_target), [-1, 1]), gamma_all_nt), [-1, maxlen]), axis = 1, keep_dims = True) #(batch, 1)
    y_6 = tf.div(y_6, len_each_seq) #(batch, 1)
    '''
    seventh term of ITFM
    ''' 
    j_first_pair = tf.constant(pad_batch_with_index, dtype = tf.int32) #(batch*seqlen, 2)
    j_first_index = tf.reshape(tf.gather_nd(jt, j_first_pair), [-1]) #(batch*seqlen)
    v_all_first_jt = tf.reshape(tf.gather(item_lookup, j_first_index, axis = 0), [-1, 1, dim_hidden]) #(batch*seqlen, 1, dim_hidden)
    #print(v_all_first_jt.shape)
    v_all_target_j = tf.reshape(tf.gather(item_lookup, tf.tile(j, [maxlen]), axis = 0), [-1, dim_hidden, 1]) #(batch*seqlen, dim_hidden, 1)
    #print(v_all_target_j.shape)
    theta_sum = tf.constant(0., shape = [batch_size, maxlen]) #(batch,seqlen)
    for each_action in range(n_action):
        s_target = tf.multiply(tf.constant([n_action for x in range(batch_size)], dtype = tf.int32), n_category) + 1 #(batch)
        s_index_pair = tf.concat([tf.reshape(st, [-1, maxlen, 1]), tf.tile(tf.reshape(s_target, [-1, 1, 1]), [1, maxlen, 1])], axis = 2) #(batch, seqlen, 2)
        theta_sum = theta_sum + tf.reshape(tf.gather_nd(theta_expanded, tf.reshape(s_index_pair, [-1, 2])), [-1, maxlen]) #(batch, seqlen)
    y_7 = tf.reduce_sum(tf.multiply(tf.reshape(tf.matmul(v_all_first_jt, v_all_target_j), [-1, maxlen]), theta_sum), axis = 1, keep_dims = True) #(batch, 1)
    y_7 = tf.div(y_7, (len_each_seq * n_action)) #(batch, 1)
    #print(y_7.shape)
    '''
    eigth term of ITFM
    '''   
    jt_pair_in_seq = tf.reshape(tf.constant(pad_1_to_2_index, dtype = tf.int32), [1, -1, 2]) #(1, sum[maxlen - 1], 2)
    jt_first_in_seq, jt_second_in_seq = tf.split(tf.tile(jt_pair_in_seq, [batch_size, 1, 1]), [1, 1], axis = 2) #(batch, sum[maxlen - 1], 1)
    batch_id = tf.tile(tf.constant([[[x]] for x in range(batch_size)], dtype = tf.int32), [1, tf.shape(jt_pair_in_seq)[1], 1]) #(batch, sum[maxlen - 1], 1)
    jt_first_in_seq = tf.concat([batch_id, jt_first_in_seq], axis = 2) #(batch, sum[maxlen - 1], 2)
    jt_second_in_seq = tf.concat([batch_id, jt_second_in_seq], axis = 2) #(batch, sum[maxlen - 1], 2)
    jt_first_index = tf.reshape(tf.gather_nd(jt, tf.reshape(jt_first_in_seq, [-1, 2])), [-1, 1]) #(batch*sum[maxlen - 1], 1)
    jt_second_index = tf.reshape(tf.gather_nd(jt, tf.reshape(jt_second_in_seq, [-1, 2])), [-1, 1]) #(batch*sum[maxlen - 1], 1)
    v_all_first_jt = tf.reshape(tf.gather(item_lookup, tf.reshape(jt_first_index, [-1]), axis = 0), [-1, 1, dim_hidden]) #((batch*sum[maxlen - 1], 1, dim_hidden)
    v_all_second_jt = tf.reshape(tf.gather(item_lookup, tf.reshape(jt_second_index, [-1]), axis = 0), [-1, dim_hidden, 1]) #((batch*sum[maxlen - 1], dim_hidden, 1)
        
    s_first_index = tf.reshape(tf.gather_nd(st, tf.reshape(jt_first_in_seq, [-1, 2])), [-1, 1]) #(batch*sum[maxlen - 1], 1)
    s_second_index = tf.reshape(tf.gather_nd(st, tf.reshape(jt_second_in_seq, [-1, 2])), [-1, 1]) #(batch*sum[maxlen - 1], 1)
    st_pair_index = tf.concat([s_first_index, s_second_index], axis = 1) #(batch*sum[maxlen - 1], 2)
    theta_all_jt = tf.reshape(tf.gather_nd(theta_expanded, st_pair_index), [-1, 1]) #(batch*sum[maxlen - 1], 1)
    y_8 = tf.reduce_sum(tf.reshape(tf.multiply(tf.reshape(tf.matmul(v_all_first_jt, v_all_second_jt), [-1, 1]), theta_all_jt), [batch_size, -1]), axis = 1, keep_dims = True) #(batch, 1)
    super_len_each_seq = tf.multiply(len_each_seq, (len_each_seq - 1)) / 2 #(batch, 1)
    y_8 = tf.div(y_8, super_len_each_seq) #(batch, 1) 
    '''
    combine all inputs and form the result of ITFM
    '''
    rating_p = y_1 + y_2 + y_3 + y_4 + y_5 + y_6 + y_7 + y_8#(batch, 1) #
    
    return rating_p, this_theta, gamma_aggregated

rating_p, matrix_theta, gamma_agg = model_AIR(pi, i, j, n, jt, nt, st, weights, biases)

'''
# Define loss and optimizer
lamda_1 = 1.0 # a big lamda prevents model from overfitting training data; a small one prevents underfitting
lamda_2 = 0.001 # a big lamda prevents model from overfitting training data; a small one prevents underfitting
lamda_3 = 0.001
l2 = sum(tf.nn.l2_loss(tf_var) for tf_var in tf.trainable_variables()) # L2 regularization to prevent overfitting

cross_entropy = tf.reduce_sum(tf.nn.softmax_cross_entropy_with_logits(labels = y_intention, logits = e, dim = -1))
attn_constraint = tf.reduce_sum(tf.square(tf.constant([1. for x in range(dim_class)]) - tf.reduce_sum(matrix_theta, axis = 1)))

loss_function_AUIA = lamda_1 * cross_entropy + lamda_2 * l2 + lamda_3 * attn_constraint
loss_function_ITFM = tf.reduce_sum(tf.square(tf.subtract(rating_p, y_rating))) #squared error
loss_function = loss_function_AUIA + loss_function_ITFM

optimizer_AIR = tf.train.AdamOptimizer(learning_rate = lr).minimize(loss_function)
#optimizer_AIR = tf.train.GradientDescentOptimizer(learning_rate = lr).minimize(loss_function_ITFM)
'''
print("TF graph loaded. Start testing now.")
# Launch the traning session

saver = tf.train.Saver()
small_batch = 100
with tf.Session() as sess:
    '''
    restore all parameters before the test starts
    '''
    saver.restore(sess, "saved_params/version5/model_6000.ckpt")
    print("Model restored.")
    
    hits = {1: 0., 5:0., 10:0., 20:0., 50:0.}
    ndcg = {1: 0., 5:0., 10:0., 20:0., 50:0.}
    ss_file = open("pred_results.txt", "w")
    for user_id in range(1, n_user + 1):
        hit_1 = []
        hit_5 = []
        hit_10 = []
        hit_20 = []
        hit_50 = []
    
        ndcg_1 = []
        ndcg_5 = []
        ndcg_10 = []
        ndcg_20 = []
        ndcg_50 = []
        test_data.next()
        test_size = len((test_data.batch_j).tolist())
        print("AIR test status>> computing ranking for user " + str(user_id) + " (test size " + str(test_size) + ")")
        '''
        get the intention of one user
        '''
        #user_gamma = sess.run([gamma_aggregated], feed_dict = {pi: numpy.tile(test_data.batch_pi, (1, small_batch))})       
        
        '''
        compute the negativge rank for J negative samples
        '''        
        rank_list_neg = []
        neg_jn = test_data.negative_sample[user_id]
        sample_id = 0
        #print(numpy.array([x[1] for x in neg_jn[sample_id : sample_id + small_batch]]).shape)
        while sample_id < len(neg_jn):
            rank = sess.run(rating_p,
                    feed_dict = {pi: numpy.tile(test_data.batch_pi, (small_batch, 1, 1)),
                                  i: numpy.tile(test_data.batch_i, (small_batch)),
                                  j: [x[0] for x in neg_jn[sample_id : sample_id + small_batch]],
                                  n: [x[1] for x in neg_jn[sample_id : sample_id + small_batch]],
                                 jt: numpy.tile(test_data.batch_jt, (small_batch, 1)),
                                 nt: numpy.tile(test_data.batch_nt, (small_batch, 1)),
                                 st: numpy.tile(test_data.batch_st, (small_batch, 1))})
            rank_list_neg += [rk[0] for rk in rank.tolist()]
            sample_id += small_batch
        '''
        compute the positive rank for positive samples
        '''
        rank = sess.run(rating_p,
                    feed_dict = {pi: numpy.tile(test_data.batch_pi, (small_batch, 1, 1)),
                                  i: numpy.tile(test_data.batch_i, (small_batch)),
                                  j: numpy.pad(test_data.batch_j, (0, small_batch - test_size), 'constant', constant_values = (0, 1)), #pad(a, (2,3), 'constant', constant_values=(4, 6))
                                  n: numpy.pad(test_data.batch_n, (0, small_batch - test_size), 'constant', constant_values = (0, 1)),
                                 jt: numpy.tile(test_data.batch_jt, (small_batch, 1)),
                                 nt: numpy.tile(test_data.batch_nt, (small_batch, 1)),
                                 st: numpy.tile(test_data.batch_st, (small_batch, 1))})            
        rank_list_pos = [rk[0] for rk in rank.tolist()[: test_size]]
        '''
        calculate hits and NDCG
        '''
        neg_r_dic = dict(zip(rank_list_neg, [x[0] for x in neg_jn]))
        rank_list_neg.sort(reverse = True)
        ddd = 0
        
        for each_rank in rank_list_pos:
            temp_list = rank_list_neg + [each_rank]
            temp_list.sort(reverse = True)
            true_rank = numpy.float(temp_list.index(each_rank) + 1)
            ndcg_score = 1.0 / math.log(true_rank + 1.0, 2)
            if each_rank >= rank_list_neg[0]:
                hit_1.append(1)
                ndcg_1.append(1.)
                intention_vec = sess.run(gamma_agg,
                    feed_dict = {pi: numpy.tile(test_data.batch_pi, (small_batch, 1, 1))})
                intention_vec = intention_vec.tolist()
                ss_file.write("User: " + str(user_id) + "\n")
                vvv = intention_vec[0][1:]
                for x in vvv[:-1]:
                    ss_file.write(str(x) + ",")
                ss_file.write("Matched Item: " + str(test_data.batch_j[ddd]) + ":" + str(each_rank) + "\n")
                ss_file.write("Other Items: " + str(neg_r_dic[rank_list_neg[1]]) + ":" + str(rank_list_neg[1]) + "," + str(neg_r_dic[rank_list_neg[2]]) + ":" + str(rank_list_neg[2]) + ","
                                                  + str(neg_r_dic[rank_list_neg[3]]) + ":" + str(rank_list_neg[3]) + "," + str(neg_r_dic[rank_list_neg[4]]) + ":" + str(rank_list_neg[4]) + "\n")
            else:
                hit_1.append(0)
                ndcg_1.append(0.)
            ddd += 1
            if each_rank >= rank_list_neg[4]:
                hit_5.append(1)
                ndcg_5.append(ndcg_score)
            else:
                hit_5.append(0)
                ndcg_5.append(0.)
            if each_rank >= rank_list_neg[9]:
                hit_10.append(1)
                ndcg_10.append(ndcg_score)
            else:
                hit_10.append(0)
                ndcg_10.append(0.)
            if each_rank >= rank_list_neg[19]:
                hit_20.append(1)
                ndcg_20.append(ndcg_score)
            else:
                hit_20.append(0)
                ndcg_20.append(0.)
            if each_rank >= rank_list_neg[49]:
                hit_50.append(1)
                ndcg_50.append(ndcg_score)
                #print("hit in 50")
            else:
                hit_50.append(0)
                ndcg_50.append(0.)           
                #print("miss in 50")
        
        h1 = float(sum(hit_1))/float(len(hit_1))
        h5 = float(sum(hit_5))/float(len(hit_5))
        h10 = float(sum(hit_10))/float(len(hit_10))
        h20 = float(sum(hit_20))/float(len(hit_20))
        h50 = float(sum(hit_50))/float(len(hit_50))
        n1 = sum(ndcg_1)/float(len(ndcg_1))
        n5 = sum(ndcg_5)/float(len(ndcg_5))
        n10 = sum(ndcg_10)/float(len(ndcg_10))
        n20 = sum(ndcg_20)/float(len(ndcg_20))
        n50 = sum(ndcg_50)/float(len(ndcg_50))
        hits[1] += h1
        hits[5] += h5
        hits[10] += h10
        hits[20] += h20
        hits[50] += h50
        ndcg[1] += n1
        ndcg[5] += n5
        ndcg[10] += n10
        ndcg[20] += n20
        ndcg[50] += n50

        if user_id % 100 == 0:
            _h1 = hits[1]/float(user_id)
            _h5 = hits[5]/float(user_id)
            _h10 = hits[10]/float(user_id)
            _h20 = hits[20]/float(user_id)
            _h50 = hits[50]/float(user_id)
            print("AIR test status>> hits@1,5,10,20,50: " + str(_h1) + ", " + str(_h5) + ", " + str(_h10) + ", " + str(_h20) + ", " + str(_h50))
            _n1 = ndcg[1]/float(user_id)
            _n5 = ndcg[5]/float(user_id)
            _n10 = ndcg[10]/float(user_id)
            _n20 = ndcg[20]/float(user_id)
            _n50 = ndcg[50]/float(user_id)
            print("AIR test status>> NDCG@1,5,10,20,50: " + str(_n1) + ", " + str(_n5) + ", " + str(_n10) + ", " + str(_n20) + ", " + str(_n50))
        
        if user_id == 1:
            tht = sess.run(matrix_theta, feed_dict = {pi: numpy.tile(test_data.batch_pi, (small_batch, 1, 1))})
            tht = tht.tolist()
            with open("attn_matrix.csv", "w") as in_file:
                for line in tht:
                    for x in line[:-1]:
                        in_file.write(str(x) + ",")
                    in_file.write(str(line[-1]) + "\n")
        
    _h1 = hits[1]/float(user_id)
    _h5 = hits[5]/float(user_id)
    _h10 = hits[10]/float(user_id)
    _h20 = hits[20]/float(user_id)
    _h50 = hits[50]/float(user_id)
    print("AIR test status>> hits@1,5,10,20,50: " + str(_h1) + ", " + str(_h5) + ", " + str(_h10) + ", " + str(_h20) + ", " + str(_h50))
    _n1 = ndcg[1]/float(user_id)
    _n5 = ndcg[5]/float(user_id)
    _n10 = ndcg[10]/float(user_id)
    _n20 = ndcg[20]/float(user_id)
    _n50 = ndcg[50]/float(user_id)
    print("AIR test status>> NDCG@1,5,10,20,50: " + str(_n1) + ", " + str(_n5) + ", " + str(_n10) + ", " + str(_n20) + ", " + str(_n50))
    ss_file.close()
    print("Status>> test finished.")